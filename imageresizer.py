import gi
import os
#import wand
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#from wand.image import Image
from gi.repository import GObject
import time
from PIL import Image


# class ListBoxRowWithData(Gtk.ListBoxRow):
#     def __init__(self, data):
#         super(Gtk.ListBoxRow, self).__init__()
#         self.data = data
#         self.add(Gtk.Label(data))

class DialogFunctions(Gtk.MessageDialog):

    def __init__(self):
        super(Gtk.MessageDialog, self).__init__()

    def errorDialog(self, title, subtitle):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, title)
        dialog.format_secondary_text(subtitle)
        dialog.run()
        dialog.destroy()


class ResizeData(object):
    def __init__(self):
        self.source_folder = ""
        self.destination_folder = ""
        self.cropimage = True
        self.paddedimage = True
        self.resizeWidth = 0
        self.resizeHeight = 0

    def setSize(self, width, height):
        self.resizeWidth = width
        self.resizeHeight = height

    def setsourceFolder(self, path):
        self.source_folder = path

    def setdestinationFolder(self, path):
        self.destination_folder = path

    def setCropImage(self, value):
        self.cropimage = bool(value)

    def setPaddedImage(self, value):
        self.paddedimage = bool(value)


class ResizeImages(GObject.GObject):
    percentage = GObject.property(type=int, default=0)
    def __init__(self, args):
        self.resizeData = args
        GObject.GObject.__init__(self)


    def setProgressBar(self, percentage):
        self.percentage = percentage

    def getProgressBar(self):
        return self.percentage

    def resizeImage(self):
        image_sizes = [
            {"width":self.resizeData.resizeWidth, "height":self.resizeData.resizeHeight}
        ]
        for size in image_sizes:
            src_files = os.listdir(self.resizeData.source_folder)
            if(len(src_files) > 0):
                dest_dir = os.path.join(self.resizeData.destination_folder, str(size['width'])+'x'+str(size['height']))
                try: 
                    print("Creating directory with name"+str(size['width'])+'x'+str(size['height']))
                    os.makedirs(dest_dir)
                except OSError:
                    if not os.path.isdir(dest_dir):
                        raise
                self.setProgressBar(1)
                noofimage = len(src_files)
                imgno = 0;
                for image in src_files:
                    if(self.resizeData.cropimage):
                        destoptdir = os.path.join(dest_dir,'crop')
                        try: 
                            print("Creating crop directory")
                            os.makedirs(destoptdir)
                        except OSError:
                            if not os.path.isdir(destoptdir):
                                raise

                        print("Resizing image \""+image+"\" using crop option to size "+str(size['width'])+'x'+str(size['height']))
                        # with Image(filename=os.path.join(self.resizeData.source_folder,image)) as img:
                        #     img.transform(resize=str(size['width'])+"x"+ str(size['height'])+"^")
                        #     img.crop(width=size['width'], height=size['height'], gravity='center')
                        #     print("Saving resized \""+image+"\" ")
                        #     img.save(filename=os.path.join(destoptdir, image))
                        self.resize_and_crop(os.path.join(self.resizeData.source_folder,image), os.path.join(destoptdir, image), (size['width'], size['height']), 'middle');

                    # if(self.resizeData.paddedimage):
                    #     destoptdir = os.path.join(dest_dir,'white_background')
                    #     try: 
                    #         print("Creating white_background directory")
                    #         os.makedirs(destoptdir)
                    #     except OSError:
                    #         if not os.path.isdir(destoptdir):
                    #             raise

                    #     #os.system('convert '+os.path.join(self.resizeData.source_folder,image)+' -resize '+ str(size['width'])+'x'+str(size['height']) +' -gravity center -background white -extent '+str(size['width'])+'x'+str(size['height'])+' '+os.path.join(destoptdir,image))
                    #     print("Resizing image \""+image+"\" using white_background option to size "+str(size['width'])+'x'+str(size['height']))
                    #     with Image(filename=os.path.join(self.resizeData.source_folder,image)) as img:
                    #         with Image(width=size['width'], height=size['height'], background=wand.color.Color("#fff")) as whiteimg:
                    #             img.transform(resize=str(size['width'])+"x"+ str(size['height']))
                    #             whiteimg.composite(img,0,0)
                    #             whiteimg.save(filename=os.path.join(destoptdir, image))                            
                            
                    #         # flattened = PythonMagick.Image(str(size['width'])+"x"+ str(size['height']), "white")
                    #         # tempimage = PythonMagick.Image(os.path.join(destoptdir, "temp_"+image))
                    #         # flattened.composite(tempimage, PythonMagick.GravityType.CenterGravity, PythonMagick.CompositeOperator.SrcOverCompositeOp)
                    #         print("Saving resized \""+image+"\" ")
                    #         # flattened.write(os.path.join(destoptdir, image))
                    #         # os.remove(os.path.join(destoptdir, "temp_"+image))

                    imgno = imgno+1
                    self.setProgressBar((100*imgno/noofimage))
                    #print(self.percentage)

    def resize_and_crop(self, img_path, modified_path, size, crop_type='top'):
	    """
	    Resize and crop an image to fit the specified size.

	    args:
	    img_path: path for the image to resize.
	    modified_path: path to store the modified image.
	    size: `(width, height)` tuple.
	    crop_type: can be 'top', 'middle' or 'bottom', depending on this
	    value, the image will cropped getting the 'top/left', 'middle' or
	    'bottom/right' of the image to fit the size.
	    raises:
	    Exception: if can not open the file in img_path of there is problems
	    to save the image.
	    ValueError: if an invalid `crop_type` is provided.
	    """
	    # If height is higher we resize vertically, if not we resize horizontally
	    img = Image.open(img_path)
	    # Get current and desired ratio for the images
	    img_ratio = img.size[0] / float(img.size[1])
	    ratio = size[0] / float(size[1])
	    #The image is scaled/cropped vertically or horizontally depending on the ratio
	    if ratio > img_ratio:
	        img = img.resize((size[0], int(round(size[0] * img.size[1] / img.size[0]))),
	            Image.ANTIALIAS)
	        # Crop in the top, middle or bottom
	        if crop_type == 'top':
	            box = (0, 0, img.size[0], size[1])
	        elif crop_type == 'middle':
	            box = (0, int(round((img.size[1] - size[1]) / 2)), img.size[0],
	                int(round((img.size[1] + size[1]) / 2)))
	        elif crop_type == 'bottom':
	            box = (0, img.size[1] - size[1], img.size[0], img.size[1])
	        else :
	            raise ValueError('ERROR: invalid value for crop_type')
	        img = img.crop(box)
	    elif ratio < img_ratio:
	        img = img.resize((int(round(size[1] * img.size[0] / img.size[1])), size[1]),
	            Image.ANTIALIAS)
	        # Crop in the top, middle or bottom
	        if crop_type == 'top':
	            box = (0, 0, size[0], img.size[1])
	        elif crop_type == 'middle':
	            box = (int(round((img.size[0] - size[0]) / 2)), 0,
	                int(round((img.size[0] + size[0]) / 2)), img.size[1])
	        elif crop_type == 'bottom':
	            box = (img.size[0] - size[0], 0, img.size[0], img.size[1])
	        else :
	            raise ValueError('ERROR: invalid value for crop_type')
	        img = img.crop(box)
	    else :
	        img = img.resize((size[0], size[1]),
	            Image.ANTIALIAS)
	    # If the scale is the same, we do not need to crop
	    img.save(modified_path)

		

class ImageResize(Gtk.Window):
    resizeObj = ""
    def __init__(self):
        Gtk.Window.__init__(self, title="Image Resize")

        self.set_border_width(10)
        self.resize_data = ResizeData()
        self.dialogObj = DialogFunctions()    

        box_outer = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(box_outer)

        # Source Folder box
        listbox = Gtk.ListBox()
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        box_outer.pack_start(listbox, True, True, 0)

        row_source = Gtk.ListBoxRow()
        hbox_source = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
        row_source.add(hbox_source)

        self.source_text_box = Gtk.Entry()
        self.source_text_box.set_property("editable", False)
        hbox_source.pack_start(self.source_text_box, False, False, 5);

        source_folder_button = Gtk.Button("Choose Source Folder")
        source_folder_button.connect("clicked", self.selectSourceFolder)
        hbox_source.pack_start(source_folder_button, False, False, 5);

        listbox.add(row_source)


        # Destination Folder box

        row_destination = Gtk.ListBoxRow()
        hbox_destination = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
        row_destination.add(hbox_destination)

        self.destination_text_box = Gtk.Entry()
        self.destination_text_box.set_property("editable", False)
        hbox_destination.pack_start(self.destination_text_box, False, False, 5);

        destination_folder_button = Gtk.Button("Choose Destination Folder")
        destination_folder_button.connect("clicked", self.selectDestinationFolder)
        hbox_destination.pack_start(destination_folder_button, False, False, 5);

        listbox.add(row_destination)



        # Add image size
        row_input = Gtk.ListBoxRow()
        hbox_input = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
        row_input.add(hbox_input)

        self.width_textbox = Gtk.Entry()
        self.width_textbox.set_property("placeholder-text", "Width")
        hbox_input.pack_start(self.width_textbox, False, False, 5);

        self.height_textbox = Gtk.Entry()
        self.height_textbox.set_property("placeholder-text", "Height")
        hbox_input.pack_start(self.height_textbox, False, False, 5);

        add_size_btn = Gtk.Button("Resize Images")
        add_size_btn.connect("clicked", self.on_add_size)
        hbox_input.pack_start(add_size_btn, False, False, 5);

        listbox.add(row_input)

        # Give option to select for resize type
        row_option = Gtk.ListBoxRow()
        hbox_option = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
        row_option.add(hbox_option)

        self.crop = Gtk.CheckButton('Crop')
        self.crop.set_active(True)
        hbox_option.pack_start(self.crop, False, False, 5);

        self.padded = Gtk.CheckButton('Padded')
        self.padded.set_active(True)
        hbox_option.pack_start(self.padded, False, False, 5);

        listbox.add(row_option)

        # Progress Bar
        progressbar_row = Gtk.ListBoxRow()
        hbox_progress = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
        progressbar_row.add(hbox_progress)

        self.progressbar = Gtk.ProgressBar()
        hbox_progress.pack_start(self.progressbar, True, True, 0)

        listbox.add(progressbar_row)


    # Add size event
    def on_add_size(self, button):
        try:
            if not self.source_text_box.get_property('text'):
                raise NameError("Source Directory Error")
            if not self.destination_text_box.get_property('text'):
                raise NameError("Destination Directory Error")

            self.resize_data.setsourceFolder(self.source_text_box.get_property('text'))
            self.resize_data.setdestinationFolder(self.destination_text_box.get_property('text'))

        except Exception, e:
            self.dialogObj.errorDialog("Empty source and destination folder", "Please set source and destination folder")
            return 

        newWidth = self.width_textbox.get_property('text')
        newHeight = self.height_textbox.get_property('text')
        try:
            newWidth = int(self.width_textbox.get_property('text'))
            newheight = int(self.height_textbox.get_property('text'))
            if(newWidth <= 0 | newheight <= 0):
                raise NameError("Option Error")
            self.resize_data.setSize(newWidth, newheight)

        except Exception, e:
            self.dialogObj.errorDialog("Invalid size", "Please enter valid height and width")
            return

        try:
            cropcheck = self.crop.get_property('active')
            paddedcheck = self.padded.get_property('active')
            if(not (cropcheck | paddedcheck)):
                raise NameError("Option Error")

            self.resize_data.setCropImage(cropcheck)
            self.resize_data.setPaddedImage(paddedcheck)

        except Exception, e:
            print(e)
            self.dialogObj.errorDialog("Invalid option", "Please select atleast one checkbox")
            return 

        self.progressbar.set_fraction(0.0)
        self.resizeObj = ResizeImages(self.resize_data)
        self.progressbar.set_show_text(True)
        self.progressbar.set_text("Started resizing images")
        self.resizeObj.connect("notify::percentage", self.on_percentage_change)
        self.resizeObj.resizeImage()
        

    # Select source folder
    def selectSourceFolder(self, button):
        dialog = Gtk.FileChooserDialog("Please choose a folder", self,
            Gtk.FileChooserAction.SELECT_FOLDER,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             "Select", Gtk.ResponseType.OK))
        dialog.set_default_size(800, 400)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.source_text_box.set_property("text",dialog.get_filename())
            print("Folder selected: " + dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    # Select destination folder
    def selectDestinationFolder(self, button):
        dialog = Gtk.FileChooserDialog("Please choose a folder", self,
            Gtk.FileChooserAction.SELECT_FOLDER,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             "Select", Gtk.ResponseType.OK))
        dialog.set_default_size(800, 400)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.destination_text_box.set_property("text",dialog.get_filename())
            print("Folder selected: " + dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def on_percentage_change(self,data, data1):
        if(self.resizeObj.percentage != 100):
            self.timeout_id = GObject.timeout_add(100, self.on_timeout, self.resizeObj.percentage)
        else:
            self.timeout_id = GObject.timeout_add(5000, self.on_timeout, self.resizeObj.percentage)

    def on_timeout(self, user_data):
        print(user_data)
        if(user_data == 1):
            progress_text = "Resizing Image - "+ str(user_data) + " completed"
            progressfraction = float(user_data)/100
            self.progressbar.set_show_text(True)
            self.progressbar.set_text(progress_text)
            self.progressbar.set_fraction(progressfraction)
        elif(user_data == 100):
            progress_text = "Finished Resizing"
            progressfraction = float(user_data)/100
            self.progressbar.set_show_text(True)
            self.progressbar.set_text(progress_text)
            self.progressbar.set_fraction(progressfraction)
        else:
            progress_text = "Resizing Image - "+ str(user_data) + " completed"
            progressfraction = float(user_data)/100
            self.progressbar.set_show_text(True)
            self.progressbar.set_text(progress_text)
            self.progressbar.set_fraction(progressfraction)

 

win = ImageResize()
# resizeObj = ResizeImages()
# resizeObj.resizeImage()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()